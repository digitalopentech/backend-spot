import requests
from sqlalchemy.orm import Session
from app.database import engine, SessionLocal
from app.models import Base, Instance

def load_data():
    url = "https://spot-bid-advisor.s3.amazonaws.com/spot-advisor-data.json"
    response = requests.get(url)
    data = response.json()

    db = SessionLocal()

    for instance_type, details in data["instance_types"].items():
        if isinstance(details, dict):
            instance = Instance(
                instance_type=instance_type,
                vcpu=details.get("cores", 0),
                memory_gib=details.get("ram_gb", 0.0),
                emr=details.get("emr", False),
                region="us-east-1",  # Ajuste conforme necessário
                savings_over_on_demand=details.get("savings_over_on_demand", 0.0),
                frequency_of_interruption=details.get("frequency_of_interruption", 0.0)
            )
            db.add(instance)
    db.commit()
    db.close()

if __name__ == "__main__":
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    load_data()
