from fastapi import FastAPI, Depends, Query
from sqlalchemy.orm import Session
from app.database import SessionLocal, engine, Base
from app.models import Instance
from app.crud import get_instances, get_instance_history, suggest_instances
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

Base.metadata.create_all(bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/instances")
def read_instances(region: str, cpu: int = Query(None), memory: float = Query(None), instance_type: str = Query(None, min_length=3), db: Session = Depends(get_db)):
    instances = get_instances(db, region, cpu, memory, instance_type)
    return instances

@app.get("/instances/history")
def read_instance_history(region: str, instance_type: str, db: Session = Depends(get_db)):
    instances = get_instance_history(db, region, instance_type)
    return instances

@app.get("/instances/suggestions")
def suggest_instance(region: str, cpu: int, memory: float, db: Session = Depends(get_db)):
    suggestions = suggest_instances(db, region, cpu, memory)
    return suggestions

# Endpoint temporário para verificar os dados carregados
@app.get("/all_instances")
def read_all_instances(db: Session = Depends(get_db)):
    instances = db.query(Instance).all()
    return instances
