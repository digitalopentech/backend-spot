from sqlalchemy import Column, Integer, String, Float, Boolean
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Instance(Base):
    __tablename__ = "instances"
    id = Column(Integer, primary_key=True, index=True)
    instance_type = Column(String, index=True)
    vcpu = Column(Integer)
    memory_gib = Column(Float)
    emr = Column(Boolean)
    savings_over_on_demand = Column(Float, nullable=True)
    frequency_of_interruption = Column(Float, nullable=True)
    update_date = Column(String, nullable=True)
    region = Column(String, nullable=False)
