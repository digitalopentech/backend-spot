from sqlalchemy.orm import Session
from .models import Instance
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def get_instances(db: Session, region: str, cpu: int = None, memory: float = None, instance_type: str = None):
    query = db.query(Instance).filter(Instance.region == region)
    if cpu:
        query = query.filter(Instance.vcpu.between(cpu * 0.8, cpu * 1.2))
    if memory:
        query = query.filter(Instance.memory_gib.between(memory * 0.8, memory * 1.2))
    if instance_type:
        query = query.filter(Instance.instance_type.like(f"%{instance_type}%"))
    return query.all()

def get_instance_history(db: Session, region: str, instance_type: str):
    logger.info(f"Querying for region: {region}, instance_type: {instance_type}")
    instances = db.query(Instance).filter(Instance.region == region, Instance.instance_type == instance_type).all()
    logger.info(f"Found {len(instances)} instances")
    return instances

def suggest_instances(db: Session, region: str, cpu: int, memory: float):
    query = db.query(Instance).filter(Instance.region == region)
    query = query.filter(Instance.vcpu.between(cpu * 0.8, cpu * 1.2))
    query = query.filter(Instance.memory_gib.between(memory * 0.8, memory * 1.2))
    suggestions = query.order_by(Instance.savings_over_on_demand.desc()).all()
    return suggestions
