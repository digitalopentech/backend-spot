# Documentação do Sistema de Busca de Instâncias

## Visão Geral
Este sistema é uma aplicação web para buscar e sugerir instâncias com base em critérios específicos como região, CPU, memória e tipo de instância. Ele consiste em um backend desenvolvido com FastAPI e um frontend em React. O backend se comunica com uma base de dados SQLAlchemy para armazenar e buscar informações sobre instâncias.

## Estrutura do Projeto

### Backend
Localizado no diretório `backend/`, contém:

- `main.py`: Arquivo principal do FastAPI que define os endpoints.
- `models.py`: Define os modelos de dados usando SQLAlchemy.
- `crud.py`: Contém funções de CRUD para interagir com a base de dados.
- `database.py`: Configuração da base de dados SQLAlchemy.
- `load_data.py`: Script para carregar dados iniciais na base de dados.

### Frontend
Localizado no diretório `frontend/`, contém:

- `src/InstanceHistory.js`: Componente React para buscar o histórico de instâncias.
- `src/InstanceSearch.js`: Componente React para buscar instâncias com base em critérios.
- `src/InstanceSuggestions.js`: Componente React para sugerir instâncias com base em critérios.

## Pré-requisitos

### Backend
- Python 3.7+
- FastAPI
- SQLAlchemy
- requests

### Frontend
- Node.js
- npm (ou yarn)

## Configuração e Instalação

### Backend
Clone o Repositório:
```bash
git clone https://github.com/usuario/repositorio.git
cd backend

###Configurar Ambiente Virtual
python -m venv venv
source venv/bin/activate  # No

Windows use: venv\Scripts\activate

Instalar Dependências:
```bash
pip install fastapi sqlalchemy uvicorn requests


Configurar Banco de Dados:

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"  # Use o caminho para o seu banco de dados

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

Carregar Dados Iniciais:
python load_data.py

Executar o servidor:
uvicorn main:app --reload


